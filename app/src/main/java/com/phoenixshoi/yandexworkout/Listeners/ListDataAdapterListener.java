package com.phoenixshoi.yandexworkout.Listeners;

/**
 * Created by phoenixshoi on 18.10.16.
 */

public interface ListDataAdapterListener {
    void onClickItem(int position);
}
