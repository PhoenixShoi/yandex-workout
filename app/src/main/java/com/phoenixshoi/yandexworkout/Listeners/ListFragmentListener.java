package com.phoenixshoi.yandexworkout.Listeners;

/**
 * Created by phoenixshoi on 18.10.16.
 */

public interface ListFragmentListener {
    void onClickItem(int position);
}
