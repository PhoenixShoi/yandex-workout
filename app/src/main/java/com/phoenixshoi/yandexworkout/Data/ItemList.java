package com.phoenixshoi.yandexworkout.Data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by phoenixshoi on 17.10.16.
 */

public class ItemList implements Parcelable {
    private int idExercise;
    private String name;
    private String helpDescription;
    private int todayCount;
    private int maxCount;

    public ItemList(int idExercise, String name, String helpDescription, int todayCount, int maxCount){
        this.idExercise = idExercise;
        this.name = name;
        this.helpDescription = helpDescription;
        this.todayCount = todayCount;
        this.maxCount = maxCount;
    }

    public int getIdExercise() {
        return idExercise;
    }

    public String getName() {
        return name;
    }

    public int getTodayCount() {
        return todayCount;
    }

    public int getMaxCount() {
        return maxCount;
    }


    public String getHelp() {
        return helpDescription;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.idExercise);
        dest.writeString(this.name);
        dest.writeString(this.helpDescription);
        dest.writeInt(this.todayCount);
        dest.writeInt(this.maxCount);
    }

    protected ItemList(Parcel in) {
        this.idExercise = in.readInt();
        this.name = in.readString();
        this.helpDescription = in.readString();
        this.todayCount = in.readInt();
        this.maxCount = in.readInt();
    }

    public static final Parcelable.Creator<ItemList> CREATOR = new Parcelable.Creator<ItemList>() {
        @Override
        public ItemList createFromParcel(Parcel source) {
            return new ItemList(source);
        }

        @Override
        public ItemList[] newArray(int size) {
            return new ItemList[size];
        }
    };
}
