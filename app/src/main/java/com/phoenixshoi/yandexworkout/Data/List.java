package com.phoenixshoi.yandexworkout.Data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by phoenixshoi on 17.10.16.
 */

public class List implements Parcelable {
    private ArrayList<ItemList> itemsList;

    public List(){
        itemsList = new ArrayList<>();
        ini();
    }

    private void ini(){
        itemsList.add(new ItemList(1,"Обычные приседания","Пlskdlkfjkdsjfkldsjlkfjlksdjlkfjlsdkjflkjsdlkjflksdkjhdskdjashdjkhaskjhdjkhsakjhdkjhaskjhdkjhsakjhdkjashkjdhkjashdjkhasjkhdkjhaskjомощь",0,0));
        itemsList.add(new ItemList(2,"На одной ноге","Помощь",0,0));
        itemsList.add(new ItemList(3,"Приседания с пульсацией","Помощь",0,0));
        itemsList.add(new ItemList(4,"Пистолет","Помощь",0,0));
        itemsList.add(new ItemList(5,"Приседания «на стул»","Помощь",0,0));
        itemsList.add(new ItemList(6,"Приседания «на стул» на носках","Помощь",0,0));
        itemsList.add(new ItemList(7,"Поза орла","Помощь",0,0));
        itemsList.add(new ItemList(8,"Гранд плие","Помощь",0,0));
        itemsList.add(new ItemList(9,"Четверочка","Помощь",0,0));
        itemsList.add(new ItemList(10,"Сумо","Помощь",0,0));
        itemsList.add(new ItemList(11,"Сумо на носках","Помощь",0,0));
        itemsList.add(new ItemList(12,"Приседание ноги накрест","Помощь",0,0));
        itemsList.add(new ItemList(13,"Приседание со скручиванием","Помощь",0,0));
    }

    public void addItem(ItemList newItem){
        itemsList.add(newItem);
    }

    public ItemList getItem(int position){
        return itemsList.get(position);
    }

    public int getSize() {
        return itemsList.size();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.itemsList);
    }

    protected List(Parcel in) {
        this.itemsList = new ArrayList<ItemList>();
        in.readList(this.itemsList, ItemList.class.getClassLoader());
    }

    public static final Parcelable.Creator<List> CREATOR = new Parcelable.Creator<List>() {
        @Override
        public List createFromParcel(Parcel source) {
            return new List(source);
        }

        @Override
        public List[] newArray(int size) {
            return new List[size];
        }
    };
}
