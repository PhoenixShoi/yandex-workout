package com.phoenixshoi.yandexworkout.Activity;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;

import com.phoenixshoi.yandexworkout.Data.List;
import com.phoenixshoi.yandexworkout.Fragments.ExerciseFragment;
import com.phoenixshoi.yandexworkout.Fragments.ListFragment;
import com.phoenixshoi.yandexworkout.Listeners.ListFragmentListener;
import com.phoenixshoi.yandexworkout.R;

public class MainActivity extends AppCompatActivity {
    private List list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startListFragment();

    }

    private void startListFragment(){
        list = new List();

        ListFragment listFragment = new ListFragment();

        listFragment.setListener(new ListFragmentListener() {
            @Override
            public void onClickItem(int position) {
                startExerciseFragment(position);
            }
        });

        Bundle bundle = new Bundle();
        bundle.putParcelable("ListData",list);

        newTransaction(listFragment,bundle,false);
    }

    private void startExerciseFragment(int position){
        if(list != null){
            ExerciseFragment exerciseFragment = new ExerciseFragment();

            Bundle bundle = new Bundle();
            bundle.putParcelable("ExerciseData",list.getItem(position));

            newTransaction(exerciseFragment,bundle,true);
        }

    }

    private void newTransaction(Fragment fragment, Bundle bundle, Boolean flagBackStack){
        fragment.setArguments(bundle);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_container,fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        if (flagBackStack)
            ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return true;
    }
}
