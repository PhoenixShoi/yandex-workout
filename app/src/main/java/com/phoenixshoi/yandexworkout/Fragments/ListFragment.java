package com.phoenixshoi.yandexworkout.Fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.phoenixshoi.yandexworkout.Adapters.ListDataAdapter;
import com.phoenixshoi.yandexworkout.Data.List;
import com.phoenixshoi.yandexworkout.Listeners.ListDataAdapterListener;
import com.phoenixshoi.yandexworkout.Listeners.ListFragmentListener;
import com.phoenixshoi.yandexworkout.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListFragment extends Fragment {
    private RecyclerView recyclerView;
    private List list;
    private ListFragmentListener listener;

    public ListFragment() {

    }

    public void setListener(ListFragmentListener listener){
        this.listener = listener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        recyclerView =  (RecyclerView) inflater.inflate(R.layout.fragment_list,container,false);

        Bundle bundle = getArguments();
        if(bundle != null){
            list = bundle.getParcelable("ListData");
        }

        if(list != null){
            ListDataAdapter listDataAdapter = new ListDataAdapter(list);
            recyclerView.setAdapter(listDataAdapter);
            listDataAdapter.setListener(new ListDataAdapterListener() {
                @Override
                public void onClickItem(int position) {
                    listener.onClickItem(position);
                }
            });
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        return recyclerView;
    }

}
