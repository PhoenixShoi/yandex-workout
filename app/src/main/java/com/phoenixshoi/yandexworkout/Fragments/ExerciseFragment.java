package com.phoenixshoi.yandexworkout.Fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.phoenixshoi.yandexworkout.Activity.MainActivity;
import com.phoenixshoi.yandexworkout.Data.ItemList;
import com.phoenixshoi.yandexworkout.R;

import java.lang.reflect.Field;


/**
 * A simple {@link Fragment} subclass.
 */
public class ExerciseFragment extends Fragment {
    private ItemList itemData;
    private View view;

    public ExerciseFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_exercise, container, false);


        Bundle bundle = getArguments();
        if(bundle != null){
            itemData = bundle.getParcelable("ExerciseData");
        }

        if(itemData != null){
        }

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.exercise_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exercise_help:

                View helpView = getView().inflate(getActivity(),R.layout.fragment_help_dialog,null);
                ImageView imageView = (ImageView) helpView.findViewById(R.id.exercise_main_image);
                TextView textView = (TextView) helpView.findViewById(R.id.help_description);


                textView.setText(itemData.getHelp());
                try {
                    Field idField = R.drawable.class.getDeclaredField("exercise"+itemData.getIdExercise());
                    imageView.setImageResource(idField.getInt(idField));
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.help_string)
                        .setCancelable(true)
                        .setView(helpView)
                        .setNegativeButton(R.string.okay,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
