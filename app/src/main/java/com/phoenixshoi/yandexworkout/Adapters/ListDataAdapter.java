package com.phoenixshoi.yandexworkout.Adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.phoenixshoi.yandexworkout.Data.List;
import com.phoenixshoi.yandexworkout.Listeners.ListDataAdapterListener;
import com.phoenixshoi.yandexworkout.R;

import java.lang.reflect.Field;


/**
 * Created by phoenixshoi on 17.10.16.
 */

public class ListDataAdapter extends RecyclerView.Adapter<ListDataAdapter.ViewHolder> {
    private List list;
    private ListDataAdapterListener listener;

    public ListDataAdapter(List list){
        this.list = list;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private CardView cardView;

        public ViewHolder(CardView v){
            super(v);
            cardView = v;
        }
    }

    public void setListener(ListDataAdapterListener listener){
        this.listener = listener;
    }

    @Override
    public ListDataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        CardView cv = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_item,parent,false);
        return new ViewHolder(cv);}

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        CardView cardView = holder.cardView;
        TextView nameItem = (TextView) cardView.findViewById(R.id.name_item);
        TextView todayCountItem = (TextView) cardView.findViewById(R.id.today_count_item);
        TextView maxCountItem = (TextView) cardView.findViewById(R.id.max_count_item);
        ImageView exerciseIcon = (ImageView) cardView.findViewById(R.id.exercise_icon);

        nameItem.setText(list.getItem(position).getName());
        todayCountItem.setText(Integer.toString(list.getItem(position).getTodayCount()));
        maxCountItem.setText(Integer.toString(list.getItem(position).getMaxCount()));

        try {
            Field idField = R.drawable.class.getDeclaredField("exercise"+list.getItem(position).getIdExercise());
            exerciseIcon.setImageResource(idField.getInt(idField));
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if((listener != null)&&(view.getId()==R.id.card_view)){
                    listener.onClickItem(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.getSize();
    }
}
